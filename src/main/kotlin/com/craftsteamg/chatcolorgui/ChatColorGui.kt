package com.craftsteamg.chatcolorgui

import com.cable.library.CableLibs
import com.cable.library.data.getData
import com.cable.library.shaded.acf.ForgeCommandManager
import com.craftsteamg.chatcolorgui.commands.ChatColorCommand
import com.craftsteamg.chatcolorgui.data.PlayerData
import net.minecraft.item.EnumDyeColor
import net.minecraftforge.common.MinecraftForge
import net.minecraftforge.event.ServerChatEvent
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent
import net.minecraftforge.fml.common.eventhandler.EventPriority
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent


const val ID = "chatcolorgui"
const val VERSION = "1.0.0"
const val NAME = "com.craftsteamg.chatcolorgui.ChatColorGui"

@Mod(
    modid = ID,
    name = NAME,
    version = VERSION,
    dependencies = "after:cable-libs;after:gooeylibs",
    acceptableRemoteVersions = "*"
)
class ChatColorGui {


    @Mod.EventHandler
    fun onPreInit(event: FMLPreInitializationEvent) {
        ForgeCommandManager(CableLibs.INSTANCE)
            .registerCommand(ChatColorCommand())
        MinecraftForge.EVENT_BUS.register(this)
    }

    @SubscribeEvent(priority = EventPriority.HIGHEST)
    fun onChat(event: ServerChatEvent) {
        val code = event.player.getData<PlayerData>().code
        code?.let {
            event.component.siblings[1].style.color = code;
        }
    }

}