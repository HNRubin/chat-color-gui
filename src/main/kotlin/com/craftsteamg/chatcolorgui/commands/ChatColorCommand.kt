package com.craftsteamg.chatcolorgui.commands

import com.cable.library.shaded.acf.BaseCommand
import com.cable.library.shaded.acf.annotation.CommandAlias
import com.cable.library.shaded.acf.annotation.CommandPermission


import com.craftsteamg.chatcolorgui.gui.openColorGui
import net.minecraft.entity.player.EntityPlayerMP

class ChatColorCommand : BaseCommand() {

    @CommandAlias("chatcolor|cc")
    @CommandPermission("chatcolor.command")
    fun runCommand(player: EntityPlayerMP) {
        openColorGui(player)
    }

}