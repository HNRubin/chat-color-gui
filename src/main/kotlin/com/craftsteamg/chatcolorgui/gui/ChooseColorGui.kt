package com.craftsteamg.chatcolorgui.gui

import ca.landonjw.gooeylibs2.api.UIManager
import ca.landonjw.gooeylibs2.api.button.GooeyButton
import ca.landonjw.gooeylibs2.api.page.GooeyPage
import ca.landonjw.gooeylibs2.api.template.Template
import ca.landonjw.gooeylibs2.api.template.types.ChestTemplate
import com.cable.library.data.getData
import com.craftsteamg.chatcolorgui.data.PlayerData
import net.minecraft.entity.player.EntityPlayerMP
import net.minecraft.init.Blocks
import net.minecraft.item.EnumDyeColor
import net.minecraft.item.ItemStack
import net.minecraft.util.text.TextFormatting
import net.minecraftforge.server.permission.PermissionAPI
import java.util.*


fun openColorGui(player: EntityPlayerMP) {
    val template = ChestTemplate.builder(2)

    val buttons = mutableListOf<GooeyButton>()
    for (color in EnumDyeColor.values()) {
        val hasPermission = player.canUseCommand(4, "chatcolor." + color.chatColor.friendlyName)
        val button = GooeyButton.builder()
            .display(ItemStack(Blocks.STAINED_GLASS_PANE, 1, color.metadata))
            .title("${TextFormatting.WHITE}Chat Color: ${color.chatColor}${color.chatColor.friendlyName}")
            .lore(
                mutableListOf(
                    if (hasPermission)
                        "${TextFormatting.GREEN}Click to activate."
                    else
                        "${TextFormatting.RED}You do not have permission for this color."
                )
            )
        if (hasPermission)
            button.onClick { action ->
                val data = action.player.getData<PlayerData>()
                data.code = color.chatColor
                data.save(false)
                UIManager.closeUI(player)
            }
        buttons.add(button.build())
    }

    val resetButton = GooeyButton.builder()
        .display(ItemStack(Blocks.BARRIER))
        .title("${TextFormatting.RED}Remove Chat Color")
        .onClick { action ->
            val data = action.player.getData<PlayerData>()
            data.code = null
            data.save(false)
            UIManager.closeUI(player)
        }
        .build();


    template.fillFromList(buttons as List<GooeyButton>)
    template.set(1, 8, resetButton)

    UIManager.openUIForcefully(
        player,
        GooeyPage.builder()
            .title("Choose your Chat Color!")
            .template(template.build())
            .build()
    )
}