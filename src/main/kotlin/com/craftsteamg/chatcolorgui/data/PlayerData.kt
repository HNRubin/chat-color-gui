package com.craftsteamg.chatcolorgui.data

import com.cable.library.data.SimpleData
import net.minecraft.util.text.TextFormatting

class PlayerData() : SimpleData() {
    var code: TextFormatting? = null


    override val fileMode: FileMode = FileMode.SINGULAR
    override val root: String = "./config/chatcolor.json"
}